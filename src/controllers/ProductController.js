const mongoose = require('mongoose');
const Product = mongoose.model('Product');

module.exports = {
  async find(req, res) {
    const { page = 1 } = req.query;
    const product = await Product.paginate({}, {page, limit: 10});

    return res.json(product);
  },

  async create(req, res) {
    const product = await Product.create(req.body);

    return res.json(product);
  },

  async show(req, res) {
    const { id } = req.params;
    const product = await Product.findById(id);

    return res.json(product);
  },

  async update(req, res) {
    const { id } = req.params;
    const product = await Product.findByIdAndUpdate(id, req.body, { new: true });

    return res.json(product);
  },

  async delete(req, res) {
    const { id } = req.params;
    await Product.findByIdAndDelete(id);

    return res.send();
  }
}

